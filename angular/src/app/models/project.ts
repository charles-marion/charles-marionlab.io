export class Project {
    id: string;
    label: string;
    thumbnail: string;
    shortDescription: string;
    description: string;
    projectPage: string;
    sourcePage: string;
    demoPage: string;
    customer: string;
    images: string[];
    technologies: {icon: string, tooltip: string}[];

    /**
     * Create Project object
     * @param item
     */
    constructor(item: any) {
        for (let key of Object.keys(item)) {
            this[key] = item[key];
        }
    }
}
