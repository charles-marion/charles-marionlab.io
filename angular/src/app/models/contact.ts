export class Contact {
    name: string = '';
    email: string = '';
    message: string = '';
    sent = false;
}
