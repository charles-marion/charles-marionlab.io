import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './components/app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
    MdButtonModule,
    MdCardModule,
    MdDialogModule,
    MdGridListModule,
    MdInputModule,
    MdProgressSpinnerModule,
    MdTabsModule,
    MdTooltipModule
} from '@angular/material';
import { HeaderComponent } from './components/header/header.component';
import { AboutMeComponent } from './components/about-me/about-me.component';
import { Ng2PageScrollModule } from 'ng2-page-scroll';
import { RouterModule } from '@angular/router';
import { TechnicalSkillsComponent } from './components/technical-skills/technical-skills.component';
import { ModalComponent } from './components/modal/modal.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { ProjectsService } from './services/projects.service';
import { ProjectComponent } from './components/projects/project.component';
import { Angulartics2GoogleAnalytics, Angulartics2Module } from 'angulartics2';
import { ContactComponent } from './components/contact/contact.component';
import { ContactService } from './services/contact.service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        AboutMeComponent,
        TechnicalSkillsComponent,
        ModalComponent,
        ProjectsComponent,
        ProjectComponent,
        ContactComponent
    ],
    imports: [
        // Angular modules
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        RouterModule.forRoot(
            [
                {path: 'technical-skills', component: ModalComponent, data: {component: TechnicalSkillsComponent} },
                {path: 'project/:id', component: ModalComponent, data: {component: ProjectComponent} },
            ],
            { useHash: true }
        ),

        // Material modules
        MdButtonModule,
        MdTabsModule,
        MdCardModule,
        MdGridListModule,
        MdTooltipModule,
        MdDialogModule,
        MdInputModule,
        MdProgressSpinnerModule,

        /* Other imports here */
        Ng2PageScrollModule,
        Angulartics2Module.forRoot([ Angulartics2GoogleAnalytics ])
    ],
    providers: [ProjectsService, ContactService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
