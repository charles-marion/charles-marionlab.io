import { Component, ElementRef, HostListener, Inject, ViewChild } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { AboutMeComponent } from './about-me/about-me.component';
import { PageScrollConfig, PageScrollInstance, PageScrollService } from 'ng2-page-scroll';
import { DOCUMENT } from '@angular/common';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { ProjectsComponent } from './projects/projects.component';
import { Angulartics2GoogleAnalytics } from 'angulartics2';
import { ContactComponent } from './contact/contact.component';

@Component({
    selector: 'app-root',
    template: `        
        <app-header
            [activeTab]="activeTab"
            (selectedTab)="scrollTo($event)"
        ></app-header>
        <div class="container mat-typography" style="position: relative;top: -20vh;"
             [@showUp]="aboutMeAnimation">
            <app-about-me ></app-about-me>
        </div>
        <div class="container mat-typography"  style="margin-top:10px;position: relative;top: -20vh;"
             [@showUp]="projectAnimation">
            <app-projects ></app-projects>
        </div>
        <div class="container mat-typography"  style="margin-top:10px;position: relative;top: -20vh;"
             [@showUp]="contactAnimation">
            <app-contact></app-contact>
        </div>
        
        <router-outlet></router-outlet>        
    `,
    styleUrls: ['app.component.scss'],
    animations: [
        trigger('showUp', [
            state("off", style({opacity: 0, transform: 'translateX(-30px)' })),
            state("on", style({ opacity: 1,  transform: 'translateX(0)'})),
            transition('off <=> on', animate(800)),
        ]),
    ]
})
export class AppComponent {
    // Animations states
    aboutMeAnimation = 'off';
    projectAnimation = 'off';
    contactAnimation = 'off';

    // Component references (used to get position)
    @ViewChild(HeaderComponent, {read: ElementRef}) header: ElementRef;
    @ViewChild(AboutMeComponent, {read: ElementRef}) aboutMe: ElementRef;
    @ViewChild(ProjectsComponent, {read: ElementRef}) projects: ElementRef;
    @ViewChild(ContactComponent, {read: ElementRef}) contact: ElementRef;

    // Current active button
    activeTab = 0;

    // Init services
    constructor(private pageScrollService: PageScrollService,
                @Inject(DOCUMENT) private document: any,
                angulartics2GoogleAnalytics: Angulartics2GoogleAnalytics) {}

    // On scroll event, manage what is visible
    @HostListener("window:scroll")
    onWindowScroll() {
        let aboutMePos = this.aboutMe.nativeElement.getBoundingClientRect().top;
        let projectsPos = this.projects.nativeElement.getBoundingClientRect().top;
        let contactPos = this.contact.nativeElement.getBoundingClientRect().top;
        if (projectsPos < window.innerHeight * 0.3) {
            this.activeTab = 2;
        } else if (aboutMePos < window.innerHeight * 0.3) {
            this.activeTab = 1;
        } else {
            this.activeTab = 0;
        }

        this.aboutMeAnimation = (aboutMePos < window.innerHeight * 0.8) ? 'on' : 'off';
        this.projectAnimation = (projectsPos < window.innerHeight * 0.95) ? 'on' : 'off';
        this.contactAnimation = (contactPos < window.innerHeight * 0.95) ? 'on' : 'off';
    }

    /**
     * Scroll to tab index
     * @param index
     */
    scrollTo (index) {
        PageScrollConfig.defaultDuration = 250;
        PageScrollConfig.defaultScrollOffset = 100;
        switch (index) {
            case 0:
                this.pageScrollService.start(PageScrollInstance.simpleInstance(this.document, this.header.nativeElement));
                break;
            case 1:
                this.pageScrollService.start(PageScrollInstance.simpleInstance(this.document, this.aboutMe.nativeElement));
                break;
            case 2:
                this.pageScrollService.start(PageScrollInstance.simpleInstance(this.document, this.projects.nativeElement));
                break;
        }
    }
}
