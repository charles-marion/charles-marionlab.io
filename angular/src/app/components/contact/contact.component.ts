import { Component, TemplateRef, ViewChild } from '@angular/core';
import { ContactService } from '../../services/contact.service';
import { Contact } from '../../models/contact';
import { MdDialog } from '@angular/material';

/**
 * Form used to contact me
 */
@Component({
    selector: 'app-contact',
    template: `
        <md-card *ngIf="!sent">
            <md-card-header>
                <md-card-title><h1><i class="fa fa-fw fa-angle-right" aria-hidden="true"></i>Contact me</h1>
                </md-card-title>
            </md-card-header>
            <md-spinner *ngIf="loading"></md-spinner>
            <form *ngIf="!loading" (ngSubmit)="onSubmit()">
                <md-card-content>
                    <table cellspacing="0">
                        <tr>
                            <td>
                                <md-input-container>
                                    <input mdInput required placeholder="Your name" name="name"
                                           [(ngModel)]="contact.name">
                                </md-input-container>
                            </td>
                            <td>
                                <md-input-container class="example-full-width">
                                    <input mdInput required placeholder="Your email" name="email"
                                           [(ngModel)]="contact.email">
                                </md-input-container>
                            </td>
                        </tr>
                    </table>

                    <p>
                        <md-input-container>
                            <textarea mdTextareaAutosize mdAutosizeMinRows="3" mdInput placeholder="Message"  name="message"
                                      [(ngModel)]="contact.message"></textarea>
                        </md-input-container>
                    </p>

                    <button [disabled]="contact.name ==='' || contact.email === ''" type="submit" md-raised-button>
                        <i class="fa fa-paper-plane" aria-hidden="true"></i>SEND</button>

                </md-card-content>
            </form>
        </md-card>
        
        <ng-template #error>
            <h4>Unable to send message. Please contact me directly at charles.marion@hotmail.fr</h4>
        </ng-template>
        <ng-template #messageSent>
            <h4>Your message has been sent successfully.</h4>
        </ng-template>
    `,
    styleUrls: ['contact.component.scss']
})
export class ContactComponent {

    // Binding to form properties
    contact: Contact;

    // State
    loading = false;
    sent = false;

    // Templates
    @ViewChild('error') error: TemplateRef<any>;
    @ViewChild('messageSent') messageSent: TemplateRef<any>;

    // Init services
    constructor(public contactService: ContactService, private dialog: MdDialog) {
        this.contact = new Contact();
    }

    // Form submit handler
    onSubmit(event) {
        this.loading = true;
        this.contactService.send(this.contact).subscribe(() => {
            this.sent = true;
            this.dialog.open(this.messageSent);
        }, (error) => {
            this.loading = false;
            this.dialog.open(this.error);
        });
    }
}
