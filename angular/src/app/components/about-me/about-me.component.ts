import { Component } from '@angular/core';

@Component({
    selector: 'app-about-me',
    template: `
        <md-card>
            <md-card-header>
                <md-card-title><h1><i class="fa fa-fw fa-angle-right"  aria-hidden="true"></i>About me</h1></md-card-title>
            </md-card-header>
            <md-card-content>
                <p>
                    I am currently 31 years old and live in Sydney, Australia.
                </p>
                <p>
                    My main interest as a <b>Senior Full Stack Software Engineer</b> is to develop challenging
                    applications using highly efficient architectures and scalable cloud technologies.
                </p>
                <p>
                    I am self-motivated and passionate about my work. I enjoy pushing the limits of tools to 
                    successfully design and implement high-quality software.
                </p>
                <p>
                    <b>Specialties:</b>                   
                </p>
                <ul> 
                    <li>Strong expertise and hands on experience in design and implementation of infrastructure
                        using Amazon Web Services such as CloudFormation, EC2, ELB, Auto Scaling, S3,
                        CloudWatch, CloudFormation etc.</li>
                    <li>Strong experience in implementing the CI processes using BitBucket,
                        Gitlab, Jasmine, Karma, Selenium, PHPUnit.</li>
                    <li>Strong expertise with Open Source technologies like Linux, PHP, Laravel,
                        TypeScript, Angular, MySQL.</li>
                </ul>
                
                <md-grid-list cols="6" rowHeight="70px">
                    <md-grid-tile
                        *ngFor="let tool of tools"
                        [colspan]="1"
                        [rowspan]="1"
                       >
                        <button *ngIf="tool.button" md-raised-button color="accent" 
                                [mdTooltip]="tool.tooltip"
                                [routerLink]="tool.routerLink"
                        >{{tool.button}}</button>
                        <i *ngIf="tool.icon" [ngClass]="tool.icon" [class.colored]="tool.hover" (mouseover)="tool.hover = true"
                           (mouseout)="tool.hover = false" [mdTooltip]="tool.tooltip"></i>
                        <a  *ngIf="tool.image" [href]="tool.link" target="_blank">
                            <img [src]="tool.image" [mdTooltip]="tool.tooltip"/>
                        </a>
                    </md-grid-tile>
                </md-grid-list>
            </md-card-content>
            <md-card-actions>
                <a href="https://www.linkedin.com/in/chmarion">
                    <button md-raised-button color="primary"><i class="fa fa-linkedin-square" aria-hidden="true"></i>See my resume on LinkedIn</button>
                </a>
            </md-card-actions>
        </md-card>
    `,
    styleUrls: ['about-me.component.scss']
})
export class AboutMeComponent {
    // Trigger animation start
    animationState = 'off';

    // List of technologies
    tools = [
        {image: '/assets/aws-architect.png',
            link: 'https://www.certmetrics.com/amazon/public/badge.aspx?i=1&t=c&d=2017-07-01&ci=AWS00288274',
            tooltip: 'AWS Certified Solutions Architect - Associate', hover: false},
        {image: '/assets/aws-developer.png',
            link: 'https://www.certmetrics.com/amazon/public/badge.aspx?i=2&t=c&d=2017-08-19&ci=AWS00288274',
            tooltip: 'AWS Certified Developer - Associate', hover: false},
        {icon: 'devicon-angularjs-plain', tooltip: 'Angular 2+', hover: false},
        {icon: 'devicon-html5-plain', tooltip: 'HTML 5', hover: false},
        {icon: 'devicon-sass-original', tooltip: 'CSS 3 and Sass', hover: false},
        {icon: 'devicon-typescript-plain', tooltip: 'TypeScript and JavaScript ES6', hover: false},
        {icon: 'devicon-nodejs-plain', tooltip: 'NodeJS and Lambda', hover: false},
        {icon: 'devicon-phpstorm-plain', tooltip: 'PHP and PHPStorm', hover: false},
        {icon: 'devicon-laravel-plain', tooltip: 'Laravel', hover: false},
        {icon: 'devicon-docker-plain', tooltip: 'Docker', hover: false},
        {icon: 'devicon-mysql-plain', tooltip: 'MySql', hover: false},
        {button: 'More', 'tooltip': 'See the full list', routerLink: 'technical-skills'}
    ];
}
