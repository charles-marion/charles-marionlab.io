import { Component, OnDestroy, OnInit } from '@angular/core';
import { MdDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

/**
 * Open modal. The content of the modal is the component defined in the route
 */
@Component({
    selector: 'app-modal',
    template: ``,
})
export class ModalComponent implements OnInit, OnDestroy {

    // Route subscription
    subscription1: Subscription;
    subscription2: Subscription;

    // Init services
    constructor(public dialog: MdDialog, private router: Router, private route: ActivatedRoute,) {}

    // Init modal
    ngOnInit () {
        setTimeout(() => {
            this.subscription1 = this.route.params.subscribe(params => {
                this.subscription2 = this.route.data.subscribe(data => {
                    let dialogRef = this.dialog.open(data.component, {data: params});
                    dialogRef.afterClosed().subscribe(result => {
                        this.router.navigate(['']);
                    });
                });
            });
        });
    }

    // Unsub route observable
    ngOnDestroy() {
        this.subscription1.unsubscribe();
        this.subscription2.unsubscribe();
    }
}
