import { Component } from '@angular/core';

@Component({
    selector: 'app-technical-skills',
    template: `
        <md-dialog-content>
            <h2 md-dialog-title>Technical skills</h2>
            <div *ngFor="let category of categories">
                    <h4>{{category.label}}</h4>
                    <div> {{category.list}} </div>
            </div>
            <button style="float: right; margin-bottom: 10px;margin-top: 10px;"  md-raised-button color="secondary" md-dialog-close>
                <i class="fa fa-times" aria-hidden="true"></i>Close
            </button>
        </md-dialog-content>
    `,
    styles: [`h4 {margin-bottom: 0}`, `h2 {margin-top: 0}`,
    `i { margin: 0 7px 3px 0;}`]
})
export class TechnicalSkillsComponent {
    // List of technologies by category
    categories = [
        {label: 'Languages', list: 'PHP 7, JavaScript ES5/6, TypeScript, Java 7, Python 2.7, C++, C#'},
        {label: 'Web Technologies', list: 'SEO, HTML5, CSS3/SASS, Material, Bootstrap, Semantic UI, jQuery, Angular, SocketIo, ThreeJS'},
        {label: 'Mobile Technologies', list: 'Android SDK, PhoneGap, Ionic'},
        {label: 'Databases', list: 'MySQL, PostgreSQL, DynamoDB'},
        {label: 'Data Intelligence', list: 'Elastic Search'},
        {label: 'Project Continuous Integration', list: 'Agile, GIT workflow, Pipelines, Selenium, PHPUnit, Mocha, Jasmine, Karma'},
        {label: 'Software Engineering', list: 'MVC, OOP, DRY, TDD, SOA'},
        {label: 'System & Networking', list: 'UNIX, RabbitMQ, AWS (Certified Architect), Docker, OpenCT, Git, Grunt, Gulp, NPM'},
    ]
}
