import { Component, Inject } from '@angular/core';
import { ProjectsService } from '../../services/projects.service';
import { Project } from '../../models/project';
import { MD_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-project',
    template: `
        <md-dialog-content>
            <div *ngIf="!project">Unable to find project</div>
            <div *ngIf="project">
                <div class="technologies">
                    <i *ngFor="let tech of project.technologies" [ngClass]="tech.icon" [mdTooltip]="tech.tooltip" ></i>
                </div>
                <h2 md-dialog-title>{{project.label}}<span style="color:grey;" *ngIf="project.year">, {{project.year}}</span></h2>
                <h4 *ngIf="project.customer">                    
                    <a *ngIf="project.customerLink" [href]="project.customerLink" target="_blank">{{project.customer}}</a>
                    <span *ngIf="!project.customerLink">{{project.customer}}</span>
                </h4>                

                <div [innerHTML]="project.description"></div>

                <div style="margin: 10px 0px">
                    <img *ngFor="let image of project.images"
                         (click)="window.open(image, '_blank');"
                         [src]="image"/>
                </div>
                <button md-raised-button color="primary"
                        *ngIf="project.projectPage"
                        (click)="window.open(project.projectPage, '_blank');"
                ><i class="fa fa-archive" aria-hidden="true"></i>Project page</button>

                <button md-raised-button color="primary"
                        *ngIf="project.demoPage"
                        (click)="window.open(project.demoPage, '_blank');"
                ><i class="fa fa-eye" aria-hidden="true"></i>Demo page</button>

                <button md-raised-button color="primary"
                        *ngIf="project.sourcePage"
                        (click)="window.open(project.sourcePage, '_blank');"
                ><i class="fa fa-code" aria-hidden="true"></i>Source Code</button>

                <button style="float: right; margin-bottom: 10px;"  md-raised-button color="secondary" md-dialog-close>
                    <i class="fa fa-times" aria-hidden="true"></i>Close
                </button>
            </div>
        </md-dialog-content>
        
    `,
    styleUrls: ['project.component.scss']
})
export class ProjectComponent{
    // Selected Project
    project: Project;
    // Share window object to template
    window = window;

    // Init Services
    constructor(public projectService: ProjectsService,  @Inject(MD_DIALOG_DATA) private data: any) {
        this.project = this.projectService.getByKey(data.id);
    }
}
