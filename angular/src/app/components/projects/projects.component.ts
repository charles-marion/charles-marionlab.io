import { Component } from '@angular/core';
import { ProjectsService } from '../../services/projects.service';

/**
 * List of projects
 */
@Component({
    selector: 'app-projects',
    template: `
        <md-card>
            <md-card-header>
                <md-card-title>
                    <h1>
                        <i class="fa fa-fw fa-angle-right"  aria-hidden="true"></i>
                        Key Projects
                    </h1>
                </md-card-title>
            </md-card-header>
            <div class="list-group" >
                <button md-button class="list-group-item" *ngFor="let project of projectService.getAll()"
                     angulartics2On="click"
                    [routerLink]="['project', project.id]"
                >                    
                    <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    <div class="image-container">
                        <img [src]="project.thumbnail"/>
                    </div>                    
                    <h4>
                        {{project.label}}
                        <span class="technologies" >
                            <i *ngFor="let tech of project.technologies" [ngClass]="tech.icon" ></i>
                        </span>
                    </h4>
                    <div class="description-container"> {{project.shortDescription}}</div>
                    
                </button>
            </div>
        </md-card>
    `,
    styleUrls: ['projects.component.scss'],
})
export class ProjectsComponent {
    constructor(public projectService: ProjectsService) {}
}
