import {
    Component,
    ElementRef,
    EventEmitter,
    HostListener,
    Input,
    NgZone,
    OnInit,
    Output,
    ViewChild
} from '@angular/core';
import { animate, group, keyframes, style, transition, trigger } from '@angular/animations';
import Typed from 'typed.js';

@Component({
    selector: 'app-header',
    template: `
        <div class="nav-bar"
             [class.scrolled]="hostElement.nativeElement.offsetHeight < currentYPos + 60 || hostElement.nativeElement.offsetWidth < 1000">
            <md-tab-group
                [selectedIndex]="activeTab"
                (focusChange)="selectedTab.emit($event.index)">
                <md-tab label="Home"></md-tab>
                <md-tab label="About me"></md-tab>
                <md-tab label="Projects"></md-tab>
            </md-tab-group>
        </div>
        <div class="center-name" @showUp>
            <h1 #typed></h1>
            <h2 [hidden]="hideSubtitle">Experienced full stack developer</h2>
            <p>
                <button md-raised-button angulartics2On="click" [@learnMore]="buttonState" (@learnMore.done)="buttonState = !buttonState " (click)="selectedTab.emit(1)">Learn more</button>
            </p>
        </div>
    `,
    styleUrls: ['header.component.scss'],
    animations: [
        trigger('showUp', [
            transition(':enter', group([
                style({ opacity: 0}),
                animate(1500, style({ opacity: 1})),
            ])),
        ]),
        trigger('learnMore', [
            transition("* => *", [
                style({ opacity: 0.6}),
                animate(4000, keyframes([
                    style({ opacity: 0.6}),
                    style({ opacity: 1}),
                    style({ opacity: 0.6}),
                ]))
            ])
        ])
    ]
})
export class HeaderComponent implements OnInit {
    // Used to change the active tab
    @Input() activeTab;
    // Event when user click on a tab element
    @Output() selectedTab = new EventEmitter<number>();
    // Typed element
    @ViewChild('typed') typedElement: ElementRef;

    // Used to start button animation
    buttonState = true;

    // Manage subtitle visibility
    hideSubtitle = true;

    // Current position in the page
    currentYPos: number = 0;

    // Get host element reference
    constructor(public hostElement: ElementRef, private ngZone: NgZone) {
    }

    // Init  Typed animation
    ngOnInit(): void {
        this.ngZone.runOutsideAngular(() => {
            new Typed(this.typedElement.nativeElement, {
                strings: [
                    'Welcome to my website!',
                    `I'm an experienced<br/>PHP back end developer`,
                    `I'm an experienced<br/>Angular developer`,
                    `I'm a certified<br/>AWS architect`,
                    `I'm Charles Marion,<br/> a full stack developer`],
                typeSpeed: 60,
                backSpeed: 20,
                showCursor: false,
                smartBackspace: true,
                loop: false
            });
        });
    }

    // Get current position
    @HostListener("window:scroll")
    @HostListener("window:resize")
    onWindowScroll() {
        if (window.pageYOffset !== 0) {
            this.currentYPos = window.pageYOffset;
        }
    }
}
