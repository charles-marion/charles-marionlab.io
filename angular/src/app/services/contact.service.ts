import { Injectable } from '@angular/core';
import { Contact } from '../models/contact';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/operator/map';

/**
 * Send contact email
 */
@Injectable()
export class ContactService {
    // Inject HttpClient into service.
    constructor(private http: HttpClient) {}

    /**
     * Send email
     * @param contact
     * @returns {Observable<Object>}
     */
    send(contact: Contact): Observable<Object> {
        let data = `name=${encodeURI(contact.name)}&email=${encodeURI(contact.email)}&message=${encodeURI(contact.message)}`;

        return this.http.post("https://formspree.io/charl.marion@gmail.com", data, {
            headers: new HttpHeaders().set('Accept', 'application/json').set('Content-Type', 'application/x-www-form-urlencoded')});
    }
}