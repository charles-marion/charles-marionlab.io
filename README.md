# Profile

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.0-beta.2.

## Development server

Run `yarn start` for a dev server. Navigate to `http://localhost:4200/`.

## Build

The build is managed by Gitlab pipeline. See .gitlab-ci.yml.